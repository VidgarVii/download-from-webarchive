require 'mechanize'
# Download site from web.archive

URL = '_____________'.freeze

class Wget
  # options {:link_regex, home_dir:}
  def initialize(url, options={})
    @url = url
    @options = options
    @agent = Mechanize.new
  end

  def get_page
    return false if @agent.head(@url).header['x-archive-orig-x-cache']

    @agent.get @url
  rescue => e
    puts e
  end

  def links
    get_page.links_with(href: @options[:link_regex])
  end

  def download
    split_link
    @direction.each do |link, dirs|
      unless dirs.nil?
        dirs.each do |dir|
          Dir.mkdir(dir) unless Dir.exist?(dir)
          Dir.chdir(dir)
        end
      end
      begin
        page = link.click
        page.save 'index.html'
      rescue => e
        puts e
      end
      Dir.chdir(@options[:home_dir])
    end
  end

  def split_link
    @direction = {}
    links.each do |link|
      path = link.href.split('http://hochurebenka.net/')[1]
      dirs = path.split('/') if path
      @direction[link] = dirs
    end
    @direction
  end
end

# wget = Wget.new(URL, options = { link_regex: /example.net/, home_dir: 'where return (directory site)'})
# wget.download

